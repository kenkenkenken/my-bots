# my-bots

## Getting started

This project lets you add, update, delete and view all bots that you have. Each bot has a name and a purpose.

This project uses TypeORM (mySQL) in storing bots.

## Running the project locally

To run this project, make sure you have the following installed in your machine:
- at least mySQL Community Server 5.7
- node version >= 14.x
- npm version >= 6.x

Run the following commands after cloning to start using the app:
- make sure your mySQL server is up and running
- npm install 
- npm run dev

## Current features

- save: create/update bots
- del: permanently delete your bots
- edit: toggles the current card to enable updating of bots
- integration with dicebear for random avatars based on provided name and purpose (as seeds)

## Not scoped / To be implemented
- name and purpose validation
- basic auth via jwt to allow users to be associated with his/her specific bots
- responsive design (mobile view)