import React, { Component } from 'react';
import './style.css';
import BotDashboard from "./Components/BotDashboard";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>My Bots</h2>
        </div>
        <BotDashboard />
      </div>
    );
  }
}

export default App;
