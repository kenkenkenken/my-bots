import React, {Component} from 'react';
import '../style.css'
import BotCreation from "./BotCreation";

export default class BotDetails extends Component {

	constructor(props) {
		super(props);
		this.state = { isUpdate: false };
	}

	deleteBot = (id) => {
		const requestOptions = {
			method: 'DELETE',
			headers: { 'Content-Type': 'application/json' }
		};
		fetch('/bots/' + id, requestOptions)
			.then((res) =>  {
				return res.json();
			}).then((json) => {
			this.props.getBots();
		});
	}

	toggleUpdateBot = (showUpdateBot) => {
		this.setState({ isUpdate: showUpdateBot });
	}

	render() {
		if (this.state.isUpdate) {
			return (
				<BotCreation bot={this.props.bot} getBots={this.props.getBots} toggleUpdateBot={this.toggleUpdateBot}/>
			);
		} else {
			return (
				<div className="card">
					<img className="avatar" src={this.props.bot.avatarSeedUrl} alt="avatar" />
					<div className="container">
						<h4>Name: <b>{this.props.bot.name}</b></h4>
						<p>Purpose: {this.props.bot.purpose}</p>
					</div>
					<a href="#" className="close" onClick={() => this.deleteBot(this.props.bot.id)}>del</a>
					<a href="#" className="edit" onClick={() => this.toggleUpdateBot(true)}>edit</a>
				</div>
			);
		}
	}
}
