import React, { Component } from 'react';
import '../style.css'
import BotDetails from "./BotDetails";
import BotCreation from "./BotCreation";

export default class BotDashboard extends Component {
	constructor(props) {
		super(props);
		this.state = { bots: [], botsAreLoading: true };
	}

	componentDidMount() {
		this.getBots();
	}

	getBots = () => {
		const requestOptions = {
			method: 'GET',
			headers: { 'Content-Type': 'application/json' },
		};
		fetch("/bots", requestOptions)
			.then((res) =>  {
				return res.json();
			}).then((json) => {
				this.setState({ bots: json });
			});
	}

	deleteBot = (botId) => {
		const requestOptions = {
			method: 'DELETE',
			headers: { 'Content-Type': 'application/json' }
		};
		fetch('/bots/' + botId, requestOptions)
			.then((res) =>  {
				this.setState({ botsAreLoading: false })
			}).then((json) => {
			console.log(json);
		});
	}

	render() {
		let myBots = this.state.bots.map((bot, index)=>{
			return <BotDetails bot={bot} getBots={this.getBots} key={index}/>
		})

		return (
			<div>
				<BotCreation getBots={this.getBots}/>
				{ myBots }
			</div>
		);
	}
}
