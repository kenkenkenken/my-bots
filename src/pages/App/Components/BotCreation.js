import React, {Component} from 'react';
import '../style.css'

export default class BotCreation extends Component {
	constructor(props) {
		super(props);
		if (props.bot) {
			this.state = { id: props.bot.id, avatarSeedUrl: props.bot.avatarSeedUrl, name: props.bot.name, purpose: props.bot.purpose };
		} else {
			this.state = { avatarSeedUrl: 'https://avatars.dicebear.com/api/bottts/a.svg', name: '', purpose: '' };
		}
	}

	handleBotNameChange = (event) => {
		this.setState({ name: event.target.value });
		this.setState({ avatarSeedUrl: 'https://avatars.dicebear.com/api/bottts/' + event.target.value + '-' + this.state.purpose + '.svg' });
	}

	handleBotPurposeChange = (event) => {
		this.setState({ purpose: event.target.value });
		this.setState({ avatarSeedUrl: 'https://avatars.dicebear.com/api/bottts/' + this.state.name + '-' + event.target.value + '.svg' });
	}

	saveBot = () => {
		const bot = {
			id: this.state.id || null,
			avatarSeedUrl: this.state.avatarSeedUrl,
			name: this.state.name,
			purpose: this.state.purpose
		}
		const requestOptions = {
			method: this.state.id ? 'PUT' : 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(bot)
		};
		fetch('/bots', requestOptions)
			.then((res) =>  {
				return res.json();
			}).then((json) => {
				this.setState({ avatarSeedUrl: 'https://avatars.dicebear.com/api/bottts/a.svg', name: '', purpose: '' });
				if (this.state.id) {
					this.props.toggleUpdateBot(false);
				}
				this.props.getBots();
			});
	}

	render() {
		return (
			<div className="card">
				<img className="avatar" src={this.state.avatarSeedUrl} alt="avatar"/>
				<div className="container">
					<div>
						<input type="text" value={this.state.name} onChange={this.handleBotNameChange} size={25}/>
					</div>
					<div>
						<textarea value={this.state.purpose} onChange={this.handleBotPurposeChange}/>
					</div>
				</div>
				<a href="#" className="save" onClick={this.saveBot}>
					save
				</a>
			</div>
		);
	}
}
