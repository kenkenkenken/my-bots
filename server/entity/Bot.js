const EntitySchema = require("typeorm").EntitySchema

module.exports = new EntitySchema({
    name: "bots", // Will use table name `bot` as default behaviour.
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true,
        },
        avatarSeedUrl: {
            type: "varchar",
        },
        name: {
            type: "varchar",
        },
        purpose: {
            type: "varchar",
        },
    },
})