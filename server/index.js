'use strict';

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();
const typeorm = require('typeorm');

typeorm.createConnection({
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "",
    "database": "my-bots",
    "synchronize": true,
    "entities": [require('./entity/Bot')]
}).then(async connection => {
    // configure app to use bodyParser()
    // this will let us get the data from a POST
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    // Serve static assets
    app.use(express.static(path.resolve(__dirname, '..', 'build')));

    app.get("/bots", async function (req, res) {
        const botRepo = connection.getRepository("bots");
        const bots = await botRepo.find();
        return res.send(bots);
    })

    app.post("/bots", async function (req, res) {
        const botRepo = connection.getRepository("bots");
        const bot = await botRepo.save(req.body);
        return res.send(bot);
    })

    app.put("/bots", async function (req, res) {
        const botRepo = connection.getRepository("bots");
        const bot = await botRepo.save(req.body);
        return res.send(bot);
    })

    app.delete("/bots/:id", async function (req, res) {
        const botRepo = connection.getRepository("bots");
        const deletedBot = await botRepo.delete(req.params.id);
        return res.send(deletedBot);
    })

    let PORT = 9000;
    app.listen(PORT, () => {
        console.log(`App listening on port ${PORT}!`);
    });

    console.log("Express application is up and running on port 3000");

}).catch(error => console.log("TypeORM connection error: ", error));


